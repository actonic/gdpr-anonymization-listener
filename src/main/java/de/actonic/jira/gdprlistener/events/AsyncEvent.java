package de.actonic.jira.gdprlistener.events;

import com.atlassian.event.api.AsynchronousPreferred;
import com.atlassian.jira.event.AbstractEvent;

import java.util.Map;

@AsynchronousPreferred
public class AsyncEvent extends AbstractEvent {
    public AsyncEvent(Map<String, Object> params) {
        super(params);
    }
}
