package de.actonic.jira.gdprlistener.component.service;

import com.atlassian.event.api.EventPublisher;
import com.google.common.base.Stopwatch;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;
import de.actonic.jira.gdprlistener.events.AsyncEvent;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Slf4j
public class DummyService {

    String emitter;
    Integer taskId;
    String source;
    String target;
    String rules;
    String scope;
    String anonymizerName;
    String eventId;

    private final EventPublisher eventPublisher;

    public DummyService(String anonymizerName, Map<String, Object> params, EventPublisher eventPublisher) {
        this.emitter = ((String) params.get("emitter")) + "Answer";
        this.taskId = (Integer) params.get("taskId");
        this.source = params.containsKey("sourceUsers") ? (String) params.get("sourceUsers") : "No 'sourceUsers' parameter define";
        this.target = params.containsKey("targetUser") ? (String) params.get("targetUser") : "No 'targetUser' parameter define";
        this.rules = params.containsKey("rules") ? (String) params.get("rules") : "No 'rules' parameter define";
        this.scope = (String) params.get("scope");
        this.anonymizerName = anonymizerName;
        this.eventId = DigestUtils.sha1Hex(anonymizerName + new Date().getTime() + Math.random() * Math.random());

        this.eventPublisher = eventPublisher;
    }

    public void executeFast() {
        String statusMessage = String.format(
                "Process started from addon with next parameters:<br>" +
                        "       emitter = '%s' <br>" +
                        "        taskId = '%s' <br>" +
                        "       eventId = '%s' <br>" +
                        "         scope = '%s' <br>" +
                        "   sourceUsers = '%s' <br>" +
                        "    targetUser = '%s' <br>" +
                        "         rules = '%s' <br>" +
                        "anonymizerName = '%s'",
                emitter, taskId, eventId, scope, source, target, rules, anonymizerName
        );
        Map<String, Object> startParams = createProperties("STARTED", statusMessage);
        log.error(startParams.toString());
        eventPublisher.publish(new AsyncEvent(startParams));

        Stopwatch stopwatch = Stopwatch.createStarted();
        double sum = 0;
        for (long i = 0; i < 10_000_000; i++)
            sum += Math.sin(i);
        sum = sum * Math.random() * 10;
        long time = stopwatch.elapsed(TimeUnit.MILLISECONDS);
        log.error("Time of the fast listener work = " + time);

        statusMessage = String.format("<b>Successfully finished</b>, result = %s, takes = %sms. Also you can go to the <a href='/plugins/servlet/audit' target='_blank'>Audit log</a>", sum, time);
        Map<String, Object> finishParams = createProperties("FINISHED", statusMessage);
        log.error(finishParams.toString());
        eventPublisher.publish(new AsyncEvent(finishParams));
    }

    public void executeSlow() {
        String statusMessage = String.format(
                "Process started from addon with next parameters:<br>" +
                        "       emitter = '%s' <br>" +
                        "        taskId = '%s' <br>" +
                        "       eventId = '%s' <br>" +
                        "         scope = '%s' <br>" +
                        "   sourceUsers = '%s' <br>" +
                        "    targetUser = '%s' <br>" +
                        "         rules = '%s' <br>" +
                        "anonymizerName = '%s'",
                emitter, taskId, eventId, scope, source, target, rules, anonymizerName
        );
        Map<String, Object> startParams = createProperties("STARTED", statusMessage);
        log.error(startParams.toString());
        eventPublisher.publish(new AsyncEvent(startParams));

        Stopwatch stopwatch = Stopwatch.createStarted();
        double sum = 0;
        for (long i = 0L; i < 500_000_000L; i++) {
            sum += Math.sin(i);
            if (i % 5_000_000 == 0) {
                Integer progress = Math.round((float) i / (float) 500_000_000 * 100);
                Map<String, Object> processingParams = createProperties("PROCESSING", "Progress " + progress + " of 100 %. Current sum = " + sum);
                eventPublisher.publish(new AsyncEvent(processingParams));
            }
        }
        sum = sum * Math.random() * 10;
        long time = stopwatch.elapsed(TimeUnit.MILLISECONDS);
        log.error("Time of the slow listener work = " + time);

        statusMessage = String.format("<b>Successfully finished</b>, result = %s, takes = %sms. Also you can go to the <a href='/plugins/servlet/audit' target='_blank'>Audit log</a>", sum, time);
        Map<String, Object> finishParams = createProperties("FINISHED", statusMessage);
        log.error(finishParams.toString());
        eventPublisher.publish(new AsyncEvent(finishParams));
    }


    public Map<String, Object> createProperties(String status, String statusMessage) {
        return createProperties(emitter, taskId, eventId, anonymizerName, status, statusMessage);
    }

    public static Map<String, Object> createProperties(String emitter, Integer taskId, String eventId, String anonymizerName, String status, String statusMessage) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("emitter", emitter);
        properties.put("taskId", taskId);
        properties.put("eventId", eventId);
        properties.put("anonymizerName", anonymizerName);
        properties.put("status", status);
        properties.put("statusMessage", statusMessage);
        return properties;
    }

    public static Boolean isEmittersCorrect(String... emitters) {
        if (emitters == null) return false;
        for (String emitter : emitters) {
            if (!(StringUtils.equalsIgnoreCase(emitter, "UserAnonymizationEvent") || StringUtils.equalsIgnoreCase(emitter, "UserSearchEvent") ||
                    StringUtils.equalsIgnoreCase(emitter, "ContentAnonymizationEvent") || StringUtils.equalsIgnoreCase(emitter, "ContentSearchEvent") ||
                    StringUtils.equalsIgnoreCase(emitter, "AnyUserAnonymizationEvent") || StringUtils.equalsIgnoreCase(emitter, "AnyUserSearchEvent")))
                return false;
        }
        return true;
    }
}
