package de.actonic.jira.gdprlistener.listener;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.AbstractEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import de.actonic.jira.gdprlistener.component.service.DummyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class TestEventListenerSlow implements InitializingBean, DisposableBean {

    @ComponentImport
    private final EventPublisher eventPublisher;

    @Autowired
    public TestEventListenerSlow(EventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @EventListener
    public void onEvent(AbstractEvent event) {
        Map<String, Object> params = event.getParams();
        if (params == null || params.size() == 0) return;
        String emitter = (String) params.get("emitter");


        if (DummyService.isEmittersCorrect(emitter)) {
            DummyService service = new DummyService("Slow 3d party anonymizer", params, eventPublisher);
            service.executeSlow();
        }
    }
}
