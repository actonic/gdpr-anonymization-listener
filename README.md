## 'GDPR Anonymization Listener' test app

### Description

This app is created to show how to use events and listeners to integrate with 'GDPR (DSVGO) and security for Jira' addon (https://marketplace.atlassian.com/apps/1218962/gdpr-dsgvo-and-security-for-jira?hosting=datacenter&tab=overview).

To listen search and anonymization event you should have some listener for AbstractEvent class. <br>
Please find example in the TestEventListenerFast or TestEventListenerSlow classes.

To send responses to the Data Cleaner you should use events. To do it in right way, you should use your personal asynchronious event. <br>
Please find example in the AsyncEvent. Use EventPublisher manager to send events.

We have also DummyService class that do monkey job, just emulate some kind of work to show how you can send different type of the requests.

### Special parameters

Our events have some number of parameters: 
* emitter - used to show event type. May be UserSearchEvent, UserAnonymizerEvent, AnyUserSearchEvent, AnyUserAnonymizerEvent, ContentSearchEvent and ContentAnonymizationEvent
* taskId - used to map event to the Data Cleaner task
* scope - used to define scope of the tickets, it is JQL query in the Data Cleaner task
* sourceUsers - used to define a list of users, that you should replace. (Only for User events)
* targetUser - used to define user, that you should apply. (Only for User and Any User events)
* rules - used to define special rules in the JSON format to show what you need to find and how to replace. (Only for Content events)

Your response event also should have some parameters, some of them are very important.
In general we suggest to use 3 different types of the event - initial event, processing events (to show progress) and finish event
* initial event - is event that will show Data Cleaner that you start working on search / anonymization of the data and we should wait while you will finish
* processing event - is event, that will show users current progress of the search / anonymization and show Data Cleaner that you are still in progress. If you will not use this event, we will mark your anonymizer as TIMEOUT after 1 hour of silence
* finish event - is event, that will show that you finished process and also to provide some statistics of the changes.

Each event should have the next number of parameters:
* emitter - used to show special event name. Your response should have 'Answer' suffix in the emitter and must be created by next rule - emitter from the Data Cleaner request = "Answer", f.e. if you are trigger "UserAnonymizationEvent", your emitter should be converted to the "UserAnonymizationEventAnswer"
* taskId - used to map your response to the Data Cleaner task (you can find it in the Data Cleaner event)
* eventId - your special <b>UNIQ</b> identifier of the process. It is very important to use really uniq identifiers for this field.
* anonymizerName - it is the name of your plugin or anonymizer, or some other special identifier to show who are you.
* status - used to provide special status of your process. May be STARTED, PROCESSING, FINISHED, FAILED. If you will not send us updates, after 1 hour we will mark your anonymizer as TIMEOUT 
* statusMessage - some text for users, it may be process status, results, anything you want in the html format

### How to install

* First of all you should have jar-file of the addon. To create it please use maven commands. Use 'package -f pom.xml' commant to create jar-file or just find Maven tab, and choose GDPR Anonymization Listener -> Lifecycle -> Package
* To install addon to the Jira, open go to the Settings (in the top right corner) -> Manage apps and choose Manage apps in the list of the tabs. You should see the list of the installed addons. On this screen please find 'Upload new addon' button and then choose needed jar-file. Here is a link to the detailed instruction https://confluence.atlassian.com/upm/installing-marketplace-apps-273875715.html

### How to start Jira locally

One of the possible options to start any version of the Jira on your local environment may be:
* Docker - special tool to start applications in the containers. Here is detailed instruction how you can do it https://community.atlassian.com/t5/Jira-articles/Running-Atlassian-server-product-on-Docker/ba-p/1209665
* Using Atlassian Plugin SDK - it is special SDK for developers, but also it allows to start any kind of the Atlassian applications. Detailed instruction you can find here https://developer.atlassian.com/server/framework/atlassian-sdk/atlas-run-standalone/.
* Using Atlassian Plugin SDK and IntelliJ Idea - this is combination of special software. If you have trully configured environment, you can just open our addon in the IntelliJ Idea, go to the Maven tab, choose Plugins -> jira -> debug and it will start Jira. In the pom.xml you can specify parameters of the required Jira. Here is instruction for installing and configuring Atlassian Plugin SDK https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/

Of course, you should have installed applications, so here is also links to instructions for Docker and Atlassian Plugin SDK.
* Docker - https://docs.docker.com/engine/install/
* Atlassian Plugin SDK - https://developer.atlassian.com/server/framework/atlassian-sdk/set-up-the-atlassian-plugin-sdk-and-build-a-project/


